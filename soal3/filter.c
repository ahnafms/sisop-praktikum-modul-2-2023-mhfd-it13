#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int compare(const void *a, const void *b) { return (*(int *)b - *(int *)a); }

void removeNonMUPlayers() {
  DIR *dir;
  struct dirent *entry;
  char filename[256], team[256], *p, path[512];
  dir = opendir("./players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry->d_type == DT_REG) {
        strcpy(filename, entry->d_name);
        char *p = strtok(filename, "_");
        p = strtok(NULL, "_");
        if (p != NULL) {
          if (strcmp(p, "ManUtd") != 0) {
            sprintf(path, "./players/%s", entry->d_name);
            remove(path);
          }
        }
      }
    }
    closedir(dir);
  }
}

void createFolder() {
  char *folderName[] = {"./players/Kiper", "./players/Bek",
                        "./players/Gelandang", "./players/Penyerang"};
  for (int i = 0; i < 4; i++) {
    pid_t pid = fork();
    char *args[] = {"mkdir", "-p", folderName[i], NULL};
    if (pid == 0) {
      execvp("mkdir", args);
    } else if (pid > 0) {
      waitpid(pid, NULL, 0);
    }
  }
}

void categorizePlayers() {
  pid_t pid, create_dir;
  struct dirent *entry;

  char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"}, filename[256],
       *p, path[512];
  DIR *dir;

  for (int i = 0; i < 4; i++) {

    pid_t pid;
    pid = fork();
    dir = opendir("./players");

    if (pid == 0 && dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            sprintf(path, "./players/%s", entry->d_name);
            char folder_path[50];
            if (strcmp(positions[i], p) == 0) {
              sprintf(folder_path, "./players/%s", positions[i]);
              char *args[] = {"mv", path, folder_path, NULL};
              pid_t cid;
              cid = fork();
              if (cid == 0)
                execvp("mv", args);
              else if (cid > 0) {
                waitpid(cid, NULL, 0);
              }
            }
          }
        }
      }
    }
    else if (pid > 0){
    waitpid(pid , NULL, 0);
  }
  }
}

void Download() {
  pid_t pid = fork();

  if (pid == 0) {
    char URL[100] = "https://drive.google.com/"
                    "uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";
    char *argv[] = {"wget", "-q", "-O", "players.zip", URL, NULL};
    execvp("wget", argv);
  } else {
    int status;
    wait(&status);

    if (WIFEXITED(status)) {
      pid_t pid2 = fork();
      if (pid2 == 0) {
        char *argv[] = {"unzip", "players.zip", NULL};
        execvp(argv[0], argv);
      } else {
        int status2;
        wait(&status2);
        if (WIFEXITED(status2)) {
          pid_t pid3 = fork();
          if (pid3 == 0) {
            char *argv[] = {"rm", "players.zip", NULL};
            execvp("rm", argv);
          }
        }
      }
    }
  }
}

void buatTim(int b, int g, int p) {
  int pemain[3] = {b, g, p};
  char *positions[] = {"./players/Bek", "./players/Gelandang",
                       "./players/Penyerang"},
       filename[256];
  FILE *fp;
  char nama_file[100];
  char *home_dir = getenv("HOME");
  sprintf(nama_file, "%s/Formasi_%d-%d-%d.txt", home_dir, b, g, p);
  fp = fopen(nama_file, "w");
  fputs("./players/Kiper\nDeGea\n", fp);
  for (int i = 0; i < 3; i++) {
    char p[100];
    sprintf(p, "%s\n", positions[i]);
    fputs(p, fp);
    DIR *dir;
    int arr[10];
    struct dirent *entry;
    dir = opendir(positions[i]);
    int j = 0;
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          arr[j] = atoi(p);
          j++;
        }
      }
      closedir(dir);
      qsort(arr, j, sizeof(int), compare);
      char strings[20][10];
      for (int l = 0; l < pemain[i]; l++) {
        dir = opendir(positions[i]);
        while ((entry = readdir(dir)) != NULL) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          if (p != NULL && arr[l] == atoi(p) &&
              strcmp(filename, strings[l - 1]) != 0) {
            strcpy(strings[l], filename);
            strcat(filename, "\n");
            fputs(filename, fp);
            break;
          }
        }
      }
    }
  }
}

int main() {
  pid_t pid, download_pid, remove_pid, createdir_pid, categorize_pid,
      buatim_pid; // Variabel untuk menyimpan PID
  int status;

  pid = fork(); // Meinyimpan PID dari Child Process
  if (pid == 0) {
    Download();
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    pid = fork();
    if (pid == 0) {
      removeNonMUPlayers();
      createFolder();
      categorizePlayers();
    }
    waitpid(pid, NULL, 0);
    buatTim(5, 3, 3);
  }
  return 0;
}
