# sisop-praktikum-modul-2-2023-mh-it13
## daftar isi ##
- [anggota kelompok](#anggota-kelompok)
- [nomor 1](#nomor-1)
    - [soal  1.a](#1a)
    - [soal  1.b](#1b)
    - [soal  1.c](#1c)
    - [soal  1.d](#1d)
    - [Hasil](#Hasil)
- [nomor 2](#nomor-2)
    - [soal  2.1](#21)
    - [soal  2.2](#22)
    - [soal  2.3](#23)
    - [soal  2.4](#24)
- [nomor 3](#nomor-3)
    - [soal  3.a](#3a)
    - [soal  3.b](#3b)
    - [hasil](#3hasil)
- [nomor 4](#nomor-4)
    - [hasil](#4hasil)
- [kendala](#kendala)


## anggota kelompok

| nrp        | nama                       |
| ---------- | -------------------------- |
| 5027211038 | ahnaf musyaffa             |
| 5027211051 | wisnu adjie saka           |
| 5027211062 | anisa ghina salsabila      |

## nomor 1

Pada nomor satu ini kita diminta untuk membantu Grape-kun menyelesaikan tugas dari atasan-nya. Dan ini adalah beberapa poin yang harus kita selesaikan : 

### 1a.
Pada poin ini kita diminta mendonload file dibawah ini 
```
https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq
```
Setelah mendownload file diatas kita diminta untuk melakukan unzip terhadap file tersebut karena file kita download berupa zip

Berikut adalah code untuk melakukan download dan unzip : 
```c
    // Download file
    system("wget -O Binatang.zip 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq'");

    // Unzip file
    struct zip *zipfile = zip_open("Binatang.zip", 0, NULL);
    int num_files = zip_get_num_files(zipfile);
    for (int s = 0; s < num_files; s++) 
    {
        const char *name = zip_get_name(zipfile, s, 0);
        struct zip_file *file = zip_fopen_index(zipfile, s, 0);
        FILE *output_file = fopen(name, "wk");
        char buffer[3000];
        int num_bytes;
        while ((num_bytes = zip_fread(file, buffer, sizeof(buffer))) > 0) 
        { fwrite(buffer, sizeof(char), num_bytes, output_file); }
        fclose(output_file);
        zip_fclose(file);
    }
    zip_close(zipfile);
```

### 1b.
Pada poin kedua kita diminta untuk melakukan pemilihan acak pada file gambar yang sudah kita unzip untuk membantu Grape-kun menentukan jadwal shift penjagaan pada hewan tersebut

Berikut adalah code-nya : 
```c
// Pemilihan acak file gambar
    int random_number = rand() % 3;
    char dest_jalur[300];
    if (random_number == 0) {
       strcpy(dest_jalur, "HewanDarat/");
    } else if (random_number == 1) {
       strcpy(dest_jalur, "HewanAir/");
    } else {
       strcpy(dest_jalur, "HewanAmphibi/"); 
    }
    pclose(fp);
```

### 1c.
Pada poin ketiga ini kita diminta untuk membuat direktori untuk memilah file gambar tersebut.  Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah itu kita memindahkan file gambar hewan sesuai dengan tempat tinggalnya.

Berikut code untuk membuat direktori dan melakukan list file gambar dari folder : 
```c
// Buat direktori HewanDarat, HewanAmphibi, dan HewanAir
    system("mkdir HewanDarat");
    system("mkdir HewanAir");
    system("mkdir HewanAmphibi");

    // Dapetin list file gambar dari folder
    FILE *fp;
    char path[3000];
    fp = popen("ls .", "r");
```

Dan berikut adalah code untuk melakukan pemilahan file tersebut : 
```c
// Mindahin file gambar ke direktori sesuai kategori hewan
    while (fgets(path, sizeof(path)-1, fp) != NULL) {
        strtok(path, "\n");
        char *namafile = strrchr(path, '/');
        if (namafile == NULL) {
            namafile = path;
        } else {
            namafile++;
        }

        char dest_jalur[300];

        if (strstr(namafile, "beruang_darat") != NULL || strstr(namafile, "singa_darat") != NULL || strstr(namafile, "komodo_darat") != NULL) {
            strcpy(dest_jalur, "./HewanDarat/");
        } else if (strstr(namafile, "lele_air") != NULL || strstr(namafile, "shark_air") != NULL || strstr(namafile, "paus_air") != NULL) {
            strcpy(dest_jalur, "./HewanAir/");
        } else if (strstr(namafile, "buaya_amphibi") != NULL || strstr(namafile, "hippo_amphibi") != NULL || strstr(namafile, "lintah_amphibi") != NULL) {
            strcpy(dest_jalur, "./HewanAmphibi/");
        } else {
            continue;
        }

        strcat(dest_jalur, namafile);

        char command[300];
        sprintf(command, "mv ./%s %s", path, dest_jalur);

        system(command);
    }
```

### 1d.
Pada poin terkahir kita diminta untuk melakukan zip pada ketiga direktori yang telah kita buat sebelumnya yang berisi file yang sudah dipilah, untuk menghemat penyimpanan. 

Berikut adalah code untuk melakukan zip : 
```c
void funct_forZip(const char *dest_fileZip) {

    pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "fork error mas bro\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {

        const char *wok[] = {"zip", "-r", dest_fileZip, "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        char **wok_copy = (char **)wok;
        execvp("zip", wok_copy);
        fprintf(stderr, "zip command gagal di execute mas bro\n");
        exit(EXIT_FAILURE);
    } else {

        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            if (exit_status == 0) {
                printf("Berhasil membuat ZIP mas bro '%s'\n", dest_fileZip);
            } else {
                fprintf(stderr, "Gagal membuat ZIP mas bro '%s'\n", dest_fileZip);
            }
        } else {
            fprintf(stderr, "Error Mas Bro\n");
        }
    }
}
```
Selanjutnya code untuk memanggil zip nya dan menghapus ketiga direktori tadi serta file zip sebelumnya hasil dari download : 
```c
funct_forZip("GabunganHewan.zip");
  
    system("rm -r HewanDarat HewanAmphibi HewanAir");
    system("rm Binatang.zip");
```

### Hasil

Dan inilah hasil/output dari code diatas : 

1. Pertama, karena kita menggunakan bahasa c , kita harus melakukan kompilasi dengan menggunkan perintah dibawah ini 

![Screenshot_gcc](/uploads/6a8ec47451de5f28e809fa78a8f2799f/Screenshot_gcc.jpg)


2. Setelah itu jalankan file nya dengan perintah berikut

![Screenshot_jalanc](/uploads/182e3348ac48c40b4cf28fe0f33ad99a/Screenshot_jalanc.jpg)

3. Berikut adalah output bila perintah berhasil dijalankan 

![Screenshot_berhasil_dijalankanc](/uploads/6406b858ffe1722aa3b83f9ab302ff87/Screenshot_berhasil_dijalankanc.jpg)

4. Selanjutnya adalah output dari folder kita, ketiga direktori berhasil di zip menjadi "GabunganHewan.zip" lalu dihapus direktori yang kosong

![Screenshot_folder](/uploads/cd48ab96b4f1fbe822f3f2daf03da44a/Screenshot_folder.jpg)

5. Dan terkahir, berikut merupakan output yang diinginkan 

![Screenshot_selesai](/uploads/e09885e24fd9a08188fd91187022feaa/Screenshot_selesai.jpg)



## nomor 2

1. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

2. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

3. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

4. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

5. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
1. Tidak boleh menggunakan system()
2. Proses berjalan secara daemon
3. Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### 2.1
```C
void createFolder(){
  time_t t;
  time(&t);
  struct tm *local = localtime(&t);
  char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
  strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
  char *argv[] = {"mkdir", "-p", timeFormat, NULL};
  if(fork() == 0){
      downloadImage(timeFormat);
  } 
  else{ 
    execv("/bin/mkdir", argv);
  }
}
```
Pada fungsi createFolder memiliki tugas untuk membuat folder yang akan dilaksanakan secara daemon 30 detik sekali.
`strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);` berfungsi untuk mengubah local format menjadi format yang diinginkan. Kemudian terdapat argv yang merupakan argumen yang akan dieksekusi di parent id dengan command `execv("/bin/mkdir", argv);`. Pada child process akan dilakukan downloadImage 

### 2.2
```C
void downloadImage(char* folder_name){
    strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
    sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat); 

    current_time = time(NULL);
    current_time = (current_time % 1000) + 50;  
    sprintf(str, "%ld", current_time);

    strcat(URL, str);
    
    char *argv[] = {"wget", "-O", fileFormat, URL , NULL}; 

    pid = fork();

    if(pid == 0){
        execvp("wget", argv);
    }

    sleep(5);
}
```
Potongan kode diatas bertujuan untuk melakukan download image. 
`sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat);` kode tersebut untuk mendefine path dari file yang akan di download. Dengan folder name adalah argumen yang diterima dari createFolder dan file name adalah waktu sekarang. Kemudian terdapat argv yang berisi command yang selanjutnya akan dieksekusi pada child process dengan command `execvp("wget", argv)`

### 2.3
```C
void removeDir(char* folder_name){
  char *argv[] = {"rm", "-rf", folder_name, NULL};
  execv("/bin/rm", argv);
}

void zip(char* folder_name){
  char zip_name[50];
  char cwd[1024];
  char path[100];
  getcwd(cwd, sizeof(cwd));
  sprintf(zip_name, "%s.zip", folder_name);
  sprintf(path, "%s/%s/", cwd, folder_name); 
  char *argv[] = {"zip", "-r", zip_name, path, NULL}; 
  pid_t child_zip, child_rmdir;
  child_zip = fork();
  int status;
  if(child_zip == 0){
      execvp("zip", argv);
  }
  else{
    waitpid(child_zip, &status, 0);
    if(WIFEXITED(status)){
      child_rmdir = fork();
      if(child_rmdir == 0){
        removeDir(folder_name);
      }
    }
  }
}
```
Potongan kode diatas melakukan zip, caranya adalah dengan menambahkan command argumen pada argv dengan `zip -r time.zip, path` kemudian akan remove menggunakan waitpid(child_zip). Jadi akan menunggu zip tersebut sampai selesai kemudian melakukan remove directory. 

### 2.4 dan 2.5
```C
void terminateProgram(char *program_name, int mode, int pid){
  FILE *killer_file = fopen("killer", "w");
  if(mode == 1){
    fprintf(killer_file, "#!/bin/bash\npkill -f %s\nrm killer\n", program_name);
  }
  else {
    fprintf(killer_file, "#!/bin/bash\nkill %d\nrm killer\n", pid);
  }
  fclose(killer_file); 
  chmod("killer", 0700);
}
```
Terdapat 2 mode yaitu mode A dan mode B. Mode A yakni pada `fprintf(killer_file, "#!/bin/bash\npkill -f %s\nrm killer\n", program_name)`
command pkill -f program_name untuk menterminasi semua program yang bernama program_name 

Kemudian mode B yakni untuk menterminasi suatu pid yakni parent pid yang menjalankan while loop, sehingga while loop tersebut akan berhenti dan tidak akan membuat folder baru. Namun tetap membiarkan child process yang sedang berjalan menjalankan tugasnya hingga zip selesai.  

### 2.hasil
berikut merupakan isi dari folder setelah menjalankan lukisan.c :
![Screenshot_2023-03-24_at_22.24.53](/uploads/eadac389954d0fd4b66e8f3d7be87bf9/Screenshot_2023-03-24_at_22.24.53.png)

## nomor 3
1. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip.

2. Hapus semua pemain yang bukan dari Manchester United yang ada di directory

3. Mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang

4. Mengkategorikan anggota tim Manchester United berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Catatan : 
1. Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
2. Tidak boleh menggunakan system()
3. Tidak boleh memakai function C mkdir() ataupun rename().
4. Gunakan exec() dan fork().
5. Directory “.” dan “..” tidak termasuk yang akan dihapus.
6. Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker

### 3.1
Fungsi mengunduh : 
```
void Download() {
  pid_t pid = fork();

  if (pid == 0) {
    char URL[100] = "https://drive.google.com/"
                    "uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";
    char *argv[] = {"wget", "-q", "-O", "players.zip", URL, NULL};
    execvp("wget", argv);
  } else {
    int status;
    wait(&status);

    if (WIFEXITED(status)) {
      pid_t pid2 = fork();
      if (pid2 == 0) {
        char *argv[] = {"unzip", "players.zip", NULL};
        execvp(argv[0], argv);
      } else {
        int status2;
        wait(&status2);
        if (WIFEXITED(status2)) {
          pid_t pid3 = fork();
          if (pid3 == 0) {
            char *argv[] = {"rm", "players.zip", NULL};
            execvp("rm", argv);
          }
        }
      }
    }
  }
}

```
fungsi Download() menggunakan tiga child process untuk mengunduh, mengekstrak, dan menghapus file bernama players.zip dari URL database, sementara parent process menunggu child process selesai dan memeriksa status keluarannya. 

Untuk proses pengunduhan pertama-tama membuat child process (dijalankan dengan memanggil fungsi execvp() dengan perintah wget) menggunakan fork() sementara parent process menunggu hingga selesai.Setelah proses pengunduhan selesai, parent process memeriksa status keluaran dari child process dengan menggunakan fungsi wait() dan makro WIFEXITED(). 

Jika proses pengunduhan telah keluar dengan sukses, parent process membuat child process lain menggunakan fork() untuk menjalankan proses ekstraksi. Proses ekstraksi dijalankan dengan memanggil fungsi execvp() dengan perintah unzip. Setelah proses ekstraksi selesai, parent process memeriksa status keluaran dari child process dengan menggunakan fungsi wait() dan makro WIFEXITED(). 

Jika proses ekstraksi telah keluar dengan sukses, parent process membuat child process lain menggunakan fork() untuk menjalankan proses penghapusan. Proses penghapusan dijalankan dengan memanggil fungsi execvp() dengan perintah rm.

### 3.2
```
void removeNonMUPlayers() {
  DIR *dir;
  struct dirent *entry;
  char filename[256], team[256], *p, path[512];
  dir = opendir("./players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry->d_type == DT_REG) {
        strcpy(filename, entry->d_name);
        char *p = strtok(filename, "_");
        p = strtok(NULL, "_");
        if (p != NULL) {
          if (strcmp(p, "ManUtd") != 0) {
            sprintf(path, "./players/%s", entry->d_name);
            remove(path);
          }
        }
      }
    }
    closedir(dir);
  }
}
```
Fungsi removeNonMUPlayers berfungsi untuk menghapus semua file dalam direktori "./players" yang bukan pemain dari klub Manchester United.

Pertama-tama, fungsi ini membuka direktori "./players" dan memproses semua file yang ada di dalamnya. Setiap file akan diproses satu per satu dalam loop while.

- Fungsi readdir digunakan untuk membaca setiap file dalam direktori.

- Jika jenis file tersebut adalah regular file (DT_REG), maka nama file akan disimpan ke dalam variabel filename.

- Fungsi strtok kemudian digunakan untuk memisahkan nama file menjadi beberapa bagian dengan delimiter "_" dan menyimpan bagian pertama ke dalam variabel p.

- Bagian kedua dari nama file (yaitu, nama klub) akan disimpan kembali ke dalam variabel p.

- Jika nama klub bukan "ManUtd", maka path dari file tersebut akan disimpan ke dalam variabel path.

- Fungsi remove kemudian digunakan untuk menghapus file dengan path yang disimpan dalam variabel path.

Setelah semua file diproses, fungsi closedir digunakan untuk menutup direktori "./players".

### 3.3
```
void categorizePlayers() {
  pid_t pid, create_dir;
  struct dirent *entry;

  char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"}, filename[256],
       *p, path[512];
  DIR *dir;

  for (int i = 0; i < 4; i++) {

    pid_t pid;
    pid = fork();
    dir = opendir("./players");

    if (pid == 0 && dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            sprintf(path, "./players/%s", entry->d_name);
            char folder_path[50];
            if (strcmp(positions[i], p) == 0) {
              sprintf(folder_path, "./players/%s", positions[i]);
              char *args[] = {"mv", path, folder_path, NULL};
              pid_t cid;
              cid = fork();
              if (cid == 0)
                execvp("mv", args);
              else if (cid > 0) {
                waitpid(cid, NULL, 0);
              }
            }
          }
        }
      }
    }
    else if (pid > 0){
    waitpid(pid , NULL, 0);
  }
  }
}
```
Fungsi categorizePlayers berfungsi untuk memindahkan file pemain ke folder yang sesuai dengan posisi, yaitu "Kiper", "Bek", "Gelandang", atau "Penyerang".
Pada setiap iterasi loop for, child process akan dibuat menggunakan fungsi fork.

- Jika nilai dari pid adalah 0, maka program akan berjalan pada child process. Pada child process, direktori "./players" akan dibuka menggunakan fungsi opendir.

- Setiap file dalam direktori "./players" akan diproses satu per satu dalam loop while.

- Jika jenis file tersebut adalah regular file (DT_REG), maka nama file akan disimpan ke dalam variabel filename.

- Fungsi strtok kemudian digunakan untuk memisahkan nama file menjadi beberapa bagian dengan delimiter "_" dan menyimpan bagian kedua ke dalam variabel p.

- 	Bagian ketiga dari nama file (yaitu, posisi pemain) akan disimpan kembali ke dalam variabel p.

- Jika nama posisi pemain sesuai dengan positions[i], maka path dari file tersebut akan disimpan ke dalam variabel path.

- Fungsi sprintf kemudian digunakan untuk menyimpan path dari folder yang sesuai dengan posisi pemain ke dalam variabel folder_path

- Command mv akan dijalankan menggunakan fungsi execvp, dengan path file dan path folder yang sesuai sebagai argumen-argumen.

- Jika nilai dari cid adalah 0, maka program akan berjalan pada child process yang dibuat oleh fork dalam if (cid == 0). Pada child process ini, command mv akan dijalankan menggunakan fungsi execvp.

- Jika nilai dari cid lebih besar dari 0, maka program akan berjalan pada parent process. Pada parent process, fungsi waitpid digunakan untuk menunggu child process selesai sebelum parent process melanjutkan eksekusi

### 3.4 
```
void buatTim(int b, int g, int p) {
  int pemain[3] = {b, g, p};
  char *positions[] = {"./players/Bek", "./players/Gelandang",
                       "./players/Penyerang"},
       filename[256];
  FILE *fp;
  char nama_file[100];
  char *home_dir = getenv("HOME");
  sprintf(nama_file, "%s/Formasi_%d-%d-%d.txt", home_dir, b, g, p);
  fp = fopen(nama_file, "w");
  fputs("./players/Kiper\nDeGea\n", fp);
  for (int i = 0; i < 3; i++) {
    char p[100];
    sprintf(p, "%s\n", positions[i]);
    fputs(p, fp);
    DIR *dir;
    int arr[10];
    struct dirent *entry;
    dir = opendir(positions[i]);
    int j = 0;
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          arr[j] = atoi(p);
          j++;
        }
      }
      closedir(dir);
      qsort(arr, j, sizeof(int), compare);
      char strings[20][10];
      for (int l = 0; l < pemain[i]; l++) {
        dir = opendir(positions[i]);
        while ((entry = readdir(dir)) != NULL) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          if (p != NULL && arr[l] == atoi(p) &&
              strcmp(filename, strings[l - 1]) != 0) {
            strcpy(strings[l], filename);
            strcat(filename, "\n");
            fputs(filename, fp);
            break;
          }
        }
      }
    }
  }
}
```
Fungsi buatTim digunakan untuk membuat formasi tim sepakbola dan mencetak nama pemain sesuai dengan formasi tersebut ke dalam file teks. 

- Fungsi memiliki tiga parameter yaitu b, g, dan p yang masing-masing merepresentasikan jumlah pemain di posisi bek, gelandang, dan penyerang.

- Array pemain digunakan untuk menyimpan jumlah pemain di setiap posisi.

- Array positions berisi alamat file dari direktori pemain di setiap posisi.

- Array filename digunakan untuk menyimpan nama file yang akan dibuat.

- Variabel fp digunakan untuk menunjuk ke file yang akan dibuka.

- Fungsi membuat sebuah string nama_file yang berisi alamat direktori home user, nama file, dan jumlah pemain di setiap posisi. Variabel nama_file kemudian digunakan dalam fopen() untuk membuka file dengan mode "w" (write).

- Fungsi mencetak dua baris pertama dari formasi tim, yaitu kiper dan nama kiper ke dalam file yang dibuka.

- Fungsi melakukan perulangan for sebanyak tiga kali untuk setiap posisi.

- Setiap posisi akan dibuka direktorinya menggunakan opendir() dan diiterasi menggunakan readdir().

- Jika jenis entri direktori adalah file (DT_REG), maka nama file tersebut akan diambil untuk diolah dan didapatkan nomor pemainnya.

- Nomor pemain ini disimpan dalam array arr.

- Setelah selesai mengambil semua nomor pemain di direktori, fungsi melakukan sorting pada array arr menggunakan qsort().

- Dibuat sebuah array dua dimensi strings yang digunakan untuk menyimpan nama file dari pemain yang dipilih nantinya.

- Perulangan for selanjutnya sebanyak pemain[i] kali (sesuai dengan jumlah pemain di posisi tersebut) akan membuka kembali direktori pemain di posisi tersebut.

- Setiap nama file akan diolah kembali untuk mendapatkan nomor pemainnya.

- Jika nomor pemain tersebut sama dengan nomor yang ada di array arr, maka nama file tersebut akan dicetak ke dalam file yang dibuka.

Setelah semua pemain dicetak ke dalam file, file akan ditutup menggunakan fclose()


### 3.hasil

Untuk hasil outputnya ketika sudah dijalankan dengan perintah ggc filter.c -o filter :

![Screenshot_2023-04-08_003200](/uploads/29a9da59357d5ba3b25eda4bad205953/Screenshot_2023-04-08_003200.png)

Terlihat adanya folder players dan formasi_5-3-3.txt

Untuk folder players ada 4 buah folder lagi yang berisi bagian dari pemain :

![Screenshot_2023-04-08_004012](/uploads/6d165b82a078499757463e6c2c0c450c/Screenshot_2023-04-08_004012.png)

Untuk formasi_5-3-3.txt :

![Screenshot_2023-04-08_004633](/uploads/945d2969a8727bd3f483664a5fea3489/Screenshot_2023-04-08_004633.png)

## nomor 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

1. Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

2. Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

3. Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

4. Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
Bonus poin apabila CPU state minimum.

Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

### 4.1, 4.2, 4.3, 4.4, 4.5
```C
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>



int main(int argc, char *argv[]){
  char *cron_hours, *cron_minutes, *cron_seconds;

  cron_hours = argv[1];
  if (strcmp(cron_hours, "*") == 0) {
    cron_hours = "0";
  }

  cron_minutes = argv[2];
  if (strcmp(cron_minutes, "*") == 0) {
    cron_minutes = "0";
  }

  cron_seconds = argv[3];
  if (strcmp(cron_seconds, "*") == 0) {
    cron_seconds = "0";
  }

  int hours = atoi(cron_hours); 
  int minutes = atoi(cron_minutes); 
  int seconds = atoi(cron_seconds);

  if(hours > 23 || minutes > 59 || seconds > 59){
    printf("Invalid input value(s) for minutes, hours, or seconds\n");
    return 1;
  }

  if(access(argv[4], X_OK) == -1 ){
      fprintf(stderr, "File not found: %s\n", argv[4]);
      return 1;
    }

  time_t now = time(NULL);
  struct tm *tm_now = localtime(&now);

  struct tm tm_cron = *tm_now;
  tm_cron.tm_hour = hours;
  tm_cron.tm_min = minutes;
  tm_cron.tm_sec = seconds;

  pid_t pid = fork();

  if(pid == 0){
      while(1){

      time_t next_time = mktime(&tm_cron);

      if(next_time < now){
        next_time += 86400;
      }

      int sleep_time = (int)difftime(next_time,now);
      
      sleep(sleep_time);
      
      char *script_path = argv[4]; 
      char *args[] = {"sh", script_path, NULL};

      pid_t child_process = fork();

      if(child_process == 0){
        int result = execvp("sh", args);
        if(result == -1){
        perror("execvp");
        return EXIT_FAILURE; 
        }
      }
    }
  } 
  return 0;
}
```
cron_hours, cron_minutes, cron_seconds bertujuan untuk menyimpan argumen jam, menit, dan detik. Asterisk pada argumen memiliki default value 0. `time_t next_time = mktime(&tm_cron);` untuk membuat sebuah waktu dalam detik yang baru yang akan bertugas menjadi time selanjutnya.

Setelah itu membuat pesan error jika argumen tidak sesuai seperti format jam, menit, dan detik melebihi satuan internasional dan juga apabila file .sh tidak ditemukan.

Kemudian membuat fork untuk membuat child process yang menjalankan sleep program dan menunggu hingga waktu next_time. 
### 4.hasil
Hasil

Contoh program dijalankan:
![Screenshot_2023-03-24_at_23.10.24](/uploads/4e86744c136061cf9eb9e369dd20c4ad/Screenshot_2023-03-24_at_23.10.24.png)

Error salah format waktu:
![Screenshot_2023-03-24_at_23.14.11](/uploads/fd343bc9d2f7cfb65eccd24d3e5ba487/Screenshot_2023-03-24_at_23.14.11.png)
## kendala
1. cukup lama stuck dibagian cron karena tidak jalan-jalan tapi akhirnya bisa jalan juga
